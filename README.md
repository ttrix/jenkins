# Jenkins notes

Jenkins server needs at least 1Gb to run properly but but for best results we can select 2 or 4 Gb wich is close to a real-world / production usage.

## Jenkins installation as a Docker container

### Setting-up requirements for a fresh server (brand new server)

apt update
apt upgrade
apt install docker.io

### Installing Jenkins

Check dockerhub documentation for up-to-date commands (https://hub.docker.com/r/jenkins/jenkins/)

```
$ docker run -d -v jenkins_home:/var/jenkins_home -p 8080:8080 -p 50000:50000 --restart=on-failure jenkins/jenkins:lts-jdk17
```

port 8080 to make Jenkins accessible from browser
port 50000 this is the port where Jenkins master and worker nodes communicate

## Initialzing Jenkins

```
docker ps       # get container ID ie. c479cca7baff
docker exec -it c479cca7baff bash
cat /var/jenkins_home/secrets/initialAdminPassword      # get initial password for Jenkins
```

## Inspect Docker volume for Jenkins

```
$ docker volume inspect jenkins_home
```

## Install build tools in Jenkins

### Via installation for a plugin for the tool

Install the plugin in: Dashboard / Manage Jenkins / Plugins
Configure the plugin in: Dashboard / Manage Jenkins / Tools

### Installing tools directly on the server

Example installing npm and node in Jenkins container:

```shell
docker exec -u 0 -it <container id> bash        # connect by ssh to server (as root)
cat /etc/issue                                  # check Linux distribution used by server
apt update
apt install curl
curl -sL https://deb.nodesource.com/setup_20.x -o nodesource_setup.sh   # download the script to install nodejs on the server
ls
bash nodesource_setup.sh
apt install nodejs
node -v
npm -v
```

## Freestyle job

- The most basic job type in Jenkins
- Not suitable for production use cases

## Docker in Jenkins

We need to install additional volumes in the container running Jenkins so we need to kill the container and restart with those additional volumes.

```
# docker stop <container id>
```

Now, we need to attach the new volume to have docker commands into out Jenkins container

```
docker run -p 8080:8080 -p 50000:50000 -d \
-v jenkins_home:/var/jenkins_home \
-v /var/run/docker.sock:/var/run/docker.sock jenkins/jenkins:lts

# get the container id
docker ps

# go inside the container
docker exec -u 0 -it <container id> bash
```

From inside the container run this to fetch the latest version of Docker from the official site so we can run it from inside the container.

```
apt install curl

curl https://get.docker.com/ > dockerinstall && chmod 777 dockerinstall && ./dockerinstall

# set permissions
chmod 666 /var/run/docker.sock
```

--docker.sock file is a Unix socket file, used by the Docker daemon to communicatet with the Docker client--

### How to install newman (postman CLI)

```
https://docs.digitalocean.com/reference/doctl/how-to/install/
```

## Freestyle to Pipeline job

## Pipeline job

## Jenkinsfile syntax

## Create complete pipeline

## Multibranch pipeline

## Jenkins jobs overview

## Credentials in Jenkins

## Jenkins shared library

## Webhooks - Trigger pipeline jobs automatically

- install Jenkins plugin "Gitlab"
- go to Manage Jenkins/System, go down and look up for Gitlab section
- set:

  - Connection name (label),
  - Gitlab host URL (url for gitlab server, ie https://gitlab.com/ or company/private/self-host gitlab)
  - Credentials (add Jenkins credentials)
    - Domain: Global
    - Kind: Gitlab API token (Gitlab profile/preferences/Access Tokens/Add new token (set expiration date and name then check api) )
  - save

- go to the desired pipeline
- check the box: "Build when a change is pushed to GitLab. GitLab webhook URL: http://..."
- save

- set webhook in gitlab side:
  - go to project
  - Settings/integrations
  - look for Jenkins
  - check boxes: Enable Integration - Active and Trigger - Push
  - set 'Jenkins server URL'
  - set 'Project name'
  - set username and password to login into Jenkins server

## Dynamically increment Application version in Jenkins pipeline
